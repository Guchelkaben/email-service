package de.mm.emailservice.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ContactDTO {
    private MailChimpResponse result;
    private List<String> validationErrors = new ArrayList<>();
}
