package de.mm.emailservice.service;


import de.mm.emailservice.domain.Contact;
import de.mm.emailservice.domain.ContactDTO;
import de.mm.emailservice.domain.Email;
import de.mm.emailservice.domain.MailChimpRequest;
import de.mm.emailservice.validation.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class ContactService {

    private final EmailService emailService;

    public ContactService(EmailService emailService) {
        this.emailService = emailService;
    }

    public ContactDTO processContact(Contact contact, String origin) {
        Validator validator = new Validator<Contact>();
        Set<ConstraintViolation<Contact>> validateErrors = validator.validateObject(contact);

        ContactDTO contactDTO = new ContactDTO();

        if (validateErrors.size() == 0) {
            MailChimpRequest mailChimpRequest = MailChimpRequest.createMailChimpRequest(contact);
            contactDTO.setResult(emailService.createContact(mailChimpRequest));

            emailService.sendEmail(Email.builder()
                    .email(contact.getEmail())
                    .phone(contact.getPhone())
                    .to(origin)
                    .subject("Kontaktformular - Informationen")
                    .name(contact.getFirstName() + " " + contact.getLastName())
                    .build());

        } else {
            List<String> validationTexts = buildValidationStrings(validateErrors);
            contactDTO.setValidationErrors(validationTexts);
        }
        return contactDTO;
    }

    private List<String> buildValidationStrings(Set<ConstraintViolation<Contact>> validationErrors) {
        log.info("ValidationErrors: {}", validationErrors);
        List<String> errorString = new ArrayList<>();
        validationErrors.forEach((entry) -> {
            errorString.add(entry.getPropertyPath().toString());
        });
        return errorString;
    }
}