package de.mm.emailservice.property;

import de.mm.emailservice.domain.property.CustomerInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

@Configuration
@ConfigurationProperties(prefix = "customer")
@Getter
@Setter
public class CustomerProperty {
    List<CustomerInfo> list;

    public Optional<CustomerInfo> getCustomerInfoById(String id) {
        return list.stream()
                .filter(customerInfo -> customerInfo.getId().equals(id))
                .findFirst();
    }
}
