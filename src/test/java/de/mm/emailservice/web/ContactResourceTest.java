package de.mm.emailservice.web;

import de.mm.emailservice.domain.Contact;
import de.mm.emailservice.domain.ContactDTO;
import de.mm.emailservice.domain.MailChimpResponse;
import de.mm.emailservice.service.ContactService;
import de.mm.emailservice.web.exception.MailChimpException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ContactResourceTest {

    private ContactService contactService;
    private ContactResource contactResource;

    @BeforeEach
    void setup() {
        contactService = mock(ContactService.class);
        contactResource = new ContactResource(contactService);
    }

    @Test
    void shouldReturn200() {
        //given
        Contact contact = new Contact();
        contact.setLastName("Marvin");
        contact.setFirstName("Knut");
        contact.setEmail("Knut@web.de");

        MailChimpResponse mailChimpResponse = new MailChimpResponse();
        mailChimpResponse.setId("1");
        mailChimpResponse.setEmail_address("Marvinofeld1299@web.de");

        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setResult(mailChimpResponse);

        when(contactService.processContact(any(), anyString())).thenReturn(contactDTO);

        //when
        ResponseEntity responseEntity = contactResource.createContact(contact, "axel-beta");

        //then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void shouldReturn400() {
        //given
        Contact contact = new Contact();
        contact.setLastName("Marvin");
        contact.setFirstName("Knut");
        contact.setEmail("Knut@web.de");

        MailChimpResponse mailChimpResponse = new MailChimpResponse();
        mailChimpResponse.setId("1");
        mailChimpResponse.setEmail_address("Marvinofeld1299@web.de");

        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setResult(mailChimpResponse);
        contactDTO.setValidationErrors(Arrays.asList("lastName", "firstName"));

        when(contactService.processContact(any(), anyString())).thenReturn(contactDTO);

        //when
        ResponseEntity responseEntity = contactResource.createContact(contact, "axel-beta");

        //then
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    void shouldReturn500() {
        //given
        Contact contact = new Contact();
        contact.setLastName("Marvin");
        contact.setFirstName("Knut");
        contact.setEmail("Knut@web.de");

        MailChimpResponse mailChimpResponse = new MailChimpResponse();
        mailChimpResponse.setId("1");
        mailChimpResponse.setEmail_address("Marvinofeld1299@web.de");

        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setResult(mailChimpResponse);
        contactDTO.setValidationErrors(Arrays.asList("lastName", "firstName"));

        when(contactService.processContact(any(), anyString())).thenThrow(new MailChimpException("Broken", HttpStatus.INTERNAL_SERVER_ERROR));

        //when
        ResponseEntity responseEntity = contactResource.createContact(contact, "axel-beta");

        //then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }

    @Test
    void shouldReturnMemberAlreadyExists() {
        //given
        Contact contact = new Contact();
        contact.setLastName("Marvin");
        contact.setFirstName("Knut");
        contact.setEmail("Knut@web.de");

        MailChimpResponse mailChimpResponse = new MailChimpResponse();
        mailChimpResponse.setId("1");
        mailChimpResponse.setEmail_address("Marvinofeld1299@web.de");

        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setResult(mailChimpResponse);
        contactDTO.setValidationErrors(Arrays.asList("lastName", "firstName"));

        when(contactService.processContact(any(), anyString())).thenThrow(new MailChimpException("Member exists", HttpStatus.BAD_REQUEST));

        //when
        ResponseEntity responseEntity = contactResource.createContact(contact, "axel-beta");

        //then
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }
}
