package de.mm.emailservice.web.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public final class MailChimpException extends RuntimeException {
    private HttpStatus reason;

    public MailChimpException(String message, HttpStatus reason) {
        super(message);
        this.reason = reason;
    }
}
