package de.mm.emailservice.web;

import de.mm.emailservice.domain.Contact;
import de.mm.emailservice.domain.ContactDTO;
import de.mm.emailservice.service.ContactService;
import de.mm.emailservice.web.exception.ErrorCode;
import de.mm.emailservice.web.exception.MailChimpException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/private/contact", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(value = {"https://www.finanzen-richtig-einfach.de", "https://finanzen-richtig-einfach.de"})
public class ContactResource {

    private final ContactService contactService;

    public ContactResource(final ContactService contactService) {
        this.contactService = contactService;
    }

    @PostMapping
    public ResponseEntity createContact(@RequestBody Contact contact, @RequestHeader("X-MM-ORIGIN") String origin) {
        try {
            ContactDTO contactDTO = contactService.processContact(contact, origin);
            return contactDTO.getValidationErrors().size() == 0 ? ResponseEntity.ok(contactDTO) : ResponseEntity.badRequest().body(contactDTO);
        } catch (MailChimpException e) {
            if (e.getMessage().equals("Member exists")) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorCode.MEMBER_EXISTS);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}