package de.mm.emailservice.repository;

import de.mm.emailservice.domain.authentication.ApiKeyResponse;
import de.mm.emailservice.domain.property.CustomerInfo;
import de.mm.emailservice.web.exception.MailChimpException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Repository
@Slf4j
public class OAuthRepository {
    private RestTemplate restTemplate;
    private String rootUrl;

    public OAuthRepository(@Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}") String rootUrl) {
        this.restTemplate = new RestTemplateBuilder()
                .build();
        this.rootUrl = rootUrl;
    }

    public ApiKeyResponse getAccessToken(CustomerInfo customerInfo) {
        try {
            customerInfo.setGrant_type("client_credentials");
            HttpEntity request = new HttpEntity<>(customerInfo);
            ResponseEntity<ApiKeyResponse> apiKeyResponse = restTemplate
                    .exchange(rootUrl + "oauth/token", HttpMethod.POST, request, ApiKeyResponse.class);

            if (apiKeyResponse.getStatusCode().is2xxSuccessful()) {
                return apiKeyResponse.getBody();
            }

            throw new MailChimpException("", HttpStatus.BAD_REQUEST);
        } catch (HttpClientErrorException e) {
            log.error("OAUTH HttpClientErrorException: ", e);
            throw new MailChimpException("", HttpStatus.BAD_REQUEST);
        } catch (RestClientException e) {
            log.error("OAUTH RestClientException: ", e);
            throw new MailChimpException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
