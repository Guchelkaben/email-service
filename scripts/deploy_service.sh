export DOCKER_PASSWORD=$(cat ./env)

#connect to instance
ssh -o StrictHostKeyChecking=no -i "$AWS_SSH_KEY" ubuntu@3.126.30.117 << EOF
  sudo docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD https://290702079319.dkr.ecr.eu-central-1.amazonaws.com

  sudo docker ps -q | xargs -r sudo docker stop ;

  sudo docker pull 290702079319.dkr.ecr.eu-central-1.amazonaws.com/mm:$DOCKER_TAG

  sudo docker run -p 8080:8080 -e ENCRYPT_KEY=$ENCRYPT_KEY -d 290702079319.dkr.ecr.eu-central-1.amazonaws.com/mm:$DOCKER_TAG

  exit
EOF