package de.mm.emailservice.service;

import de.mm.emailservice.domain.Email;
import de.mm.emailservice.domain.MailChimpRequest;
import de.mm.emailservice.domain.MailChimpResponse;
import de.mm.emailservice.repository.EmailRepository;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    private final JavaMailSender emailSender;
    private final EmailRepository emailRepository;
    private static final String textTemplate = "Kontaktformular wurde ausgefüllt. \n" +
            "Kundenname: {{kunde}} \n" +
            "Kundenemail: {{email}} \n" +
            "Kundentelefonnr: {{phone}} \n";

    EmailService(final JavaMailSender emailSender, final EmailRepository emailRepository) {
        this.emailSender = emailSender;
        this.emailRepository = emailRepository;
    }

    MailChimpResponse createContact(MailChimpRequest mailChimpRequest) {
        return emailRepository.createContact(mailChimpRequest);
    }

    void sendEmail(Email email) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(emailRepository.getEmailFromOrigin(email.getTo()));
            message.setSubject(email.getSubject());
            message.setText(generateText(email));

            emailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }

    private String generateText(Email email) {
        return textTemplate
                .replace("{{kunde}}", email.getName())
                .replace("{{email}}", email.getEmail())
                .replace("{{phone}}", email.getPhone());
    }
}
