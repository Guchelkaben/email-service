package de.mm.emailservice.repository;

import de.mm.emailservice.domain.MailChimpRequest;
import de.mm.emailservice.domain.MailChimpResponse;
import de.mm.emailservice.web.exception.MailChimpException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Repository
@Slf4j
public class EmailRepository {

    private RestTemplate restTemplate;
    private String rootUrl;
    private String listId;

    //TODO maybe database?
    private Map<String, String> customers = Map.of(
            "axel-beta", "termin@finanzen-richtig-einfach.de"
    );

    public EmailRepository(@Value("${mailchimp.url}") String rootUrl, @Value("${mailchimp.key}") String apiKey, @Value("${mailchimp.username}") String username, @Value("${mailchimp.listid}") String listId) {
        this.rootUrl = rootUrl;
        this.listId = listId;
        this.restTemplate = new RestTemplateBuilder()
                .basicAuthentication(username, apiKey)
                .build();
    }

    public MailChimpResponse createContact(MailChimpRequest mailChimpRequest) {
        try {
            HttpEntity<MailChimpRequest> request = new HttpEntity<>(mailChimpRequest);

            ResponseEntity<MailChimpResponse> mailChimpResponse = restTemplate
                    .exchange(rootUrl + "lists/" + listId + "/members/", HttpMethod.POST, request, MailChimpResponse.class);

            if (mailChimpResponse.getStatusCode().is2xxSuccessful()) {
                return mailChimpResponse.getBody();
            }

            throw new MailChimpException("", HttpStatus.BAD_REQUEST);
        } catch (HttpClientErrorException e) {
            log.error("MailChimp HttpClientErrorException: ", e);
            if (e.getStatusCode() == HttpStatus.BAD_REQUEST && e.getMessage().contains("Member Exists")) {
                throw new MailChimpException("Member exists", HttpStatus.BAD_REQUEST);
            }
            throw new MailChimpException("", HttpStatus.BAD_REQUEST);
        } catch (RestClientException e) {
            log.error("MailChimp RestClientException: ", e);
            throw new MailChimpException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public String getEmailFromOrigin(String origin) {
        return customers.get(origin);
    }
}
