package de.mm.emailservice.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoogleRequest {
    private String userResponse;
}
