package de.mm.emailservice.service;

import de.mm.emailservice.domain.Contact;
import de.mm.emailservice.domain.ContactDTO;
import de.mm.emailservice.domain.ContactStatus;
import de.mm.emailservice.domain.MailChimpResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ContactServiceTest {

    private ContactService contactService;
    private EmailService emailService;

    @BeforeEach
    void setup() {
        emailService = mock(EmailService.class);
        contactService = new ContactService(emailService);
    }

    @Test
    void shouldProcessContactValid() {
        //given
        Contact contact = new Contact();
        contact.setEmail("Test@web.de");
        contact.setContactStatus(ContactStatus.subscribed);
        contact.setFirstName("Marvin");
        contact.setLastName("Knut");
        contact.setPhone("0171237612");

        MailChimpResponse mailChimpResponse = new MailChimpResponse();
        mailChimpResponse.setEmail_address("Test@web.de");
        mailChimpResponse.setId("1");
        when(emailService.createContact(any())).thenReturn(mailChimpResponse);

        //when
        ContactDTO contactDTO = contactService.processContact(contact, "axel-beta");

        //then
        assertEquals("Test@web.de", contactDTO.getResult().getEmail_address());
        assertEquals("1", contactDTO.getResult().getId());
        verify(emailService, atLeastOnce()).createContact(any());
    }

    @Test
    void shouldProcessContactInvalid() {
        //given
        Contact contact = new Contact();
        contact.setContactStatus(ContactStatus.subscribed);
        contact.setFirstName("Marvin");
        contact.setPhone("0171237612");

        MailChimpResponse mailChimpResponse = new MailChimpResponse();
        mailChimpResponse.setEmail_address("Test@web.de");
        mailChimpResponse.setId("1");
        when(emailService.createContact(any())).thenReturn(mailChimpResponse);

        //when
        ContactDTO contactDTO = contactService.processContact(contact, "axel-beta");

        //then
        assertTrue(contactDTO.getValidationErrors().contains("lastName"));
        assertTrue(contactDTO.getValidationErrors().contains("email"));
    }
}
