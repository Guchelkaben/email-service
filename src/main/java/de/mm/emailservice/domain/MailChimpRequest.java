package de.mm.emailservice.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
@Builder
public class MailChimpRequest {
    private String email_address;
    private ContactStatus status;
    private MergeFields merge_fields;

    @Getter
    @Setter
    @Builder
    private static class MergeFields {
        private String FNAME;
        private String LNAME;
        private String PHONE;
    }

    public static MailChimpRequest createMailChimpRequest(Contact contact) {
        return MailChimpRequest.builder()
                .email_address(Optional.ofNullable(contact.getEmail()).orElse(""))
                .status(Optional.ofNullable(contact.getContactStatus()).orElse(ContactStatus.unsubscribed))
                .merge_fields(MergeFields.builder()
                        .FNAME(Optional.ofNullable(contact.getFirstName()).orElse(""))
                        .LNAME(Optional.ofNullable(contact.getLastName()).orElse(""))
                        .PHONE(Optional.ofNullable(contact.getPhone()).orElse(""))
                        .build())
                .build();
    }
}
