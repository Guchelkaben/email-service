package de.mm.emailservice.web;

import de.mm.emailservice.domain.GoogleRequest;
import de.mm.emailservice.service.ICaptchaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "api/private/captcha", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(value = {"https://www.finanzen-richtig-einfach.de", "https://finanzen-richtig-einfach.de"})
public class CaptchaResource {

    private final ICaptchaService captchaService;

    CaptchaResource(ICaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    @PostMapping
    public ResponseEntity verifyCaptcha(@RequestBody GoogleRequest request) {
        Optional<String> response = Optional.ofNullable(request.getUserResponse());

        try {
            if (response.isPresent()) {
                captchaService.processResponse(response.get());
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
