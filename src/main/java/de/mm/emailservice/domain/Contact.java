package de.mm.emailservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class Contact {

    @Size(min = 3, max = 50, message = "Size need to be between {min} and {max}")
    private String phone;

    @Size(min = 3, max = 50, message = "Size need to be between {min} and {max}")
    private String firstName;

    @Size(min = 3, max = 50, message = "Size need to be between {min} and {max}")
    @NotNull(message = "Required")
    private String lastName;

    @Size(min = 3, max = 50, message = "Size need to be between {min} and {max}")
    @Email(message = "Email is not valid")
    @NotNull(message = "Required")
    private String email;

    private ContactStatus contactStatus;
}
