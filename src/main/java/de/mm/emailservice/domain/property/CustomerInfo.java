package de.mm.emailservice.domain.property;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerInfo {
    private String id;
    private String client_secret;
    private String client_id;
    private String audience;
    private String grant_type;
}