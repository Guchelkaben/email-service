package de.mm.emailservice.domain;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Email {
    private String to;
    private String subject;
    private String name;
    private String phone;
    private String email;
}
