yum -y install python-pip

pip install awscli

mkdir -p ~/.aws/credentials/

cp "$AWS_CREDENTIALS" ~/.aws/credentials/credentials

export AWS_SHARED_CREDENTIALS_FILE=~/.aws/credentials/credentials

aws ecr get-login | cut -d ' ' -f6 > ./env