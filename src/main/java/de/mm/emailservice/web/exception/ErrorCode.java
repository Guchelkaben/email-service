package de.mm.emailservice.web.exception;

public enum ErrorCode {
    MEMBER_EXISTS(1, "Member already exists")
    ;
    private int code;
    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
