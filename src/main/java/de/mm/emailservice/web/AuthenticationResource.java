package de.mm.emailservice.web;

import de.mm.emailservice.service.AuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/public/authentication", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(value = {"https://www.finanzen-richtig-einfach.de", "https://finanzen-richtig-einfach.de"})
public class AuthenticationResource {

    private AuthenticationService authenticationService;

    AuthenticationResource(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @GetMapping
    public ResponseEntity getApiKey(@RequestHeader(value = "X-MM-ORIGIN") String customerId) {
        try {
            return authenticationService.getApiToken(customerId.toLowerCase())
                    .map(ResponseEntity::ok)
                    .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
