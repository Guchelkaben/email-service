package de.mm.emailservice.service;

import de.mm.emailservice.domain.authentication.ApiKeyResponse;
import de.mm.emailservice.property.CustomerProperty;
import de.mm.emailservice.repository.OAuthRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationService {

    private final CustomerProperty customerProperty;
    private OAuthRepository oAuthRepository;

    public AuthenticationService(OAuthRepository oAuthRepository, CustomerProperty customerProperty) {
        this.oAuthRepository = oAuthRepository;
        this.customerProperty = customerProperty;
    }

    public Optional<ApiKeyResponse> getApiToken(String customerId) {
        return customerProperty.getCustomerInfoById(customerId)
                .map(oAuthRepository::getAccessToken);
    }
}
