package de.mm.emailservice.domain.authentication;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiKeyResponse {
    private String access_token;
    private String token_type;
}
