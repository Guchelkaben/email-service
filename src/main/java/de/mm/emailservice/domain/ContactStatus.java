package de.mm.emailservice.domain;

public enum ContactStatus {
    subscribed, pending, unsubscribed
}
